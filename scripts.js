/*
* Juan Carlos Duran.
* June 20, 2016
* qheolet@gmail.com
*/

(function () {
    //List of Element
    var element = {
        input: document.getElementById('t-input'),
        addButton: document.getElementById('t-button'),
        listOfInput: document.getElementsByClassName('t-list-of-input'),
        submit: document.getElementById('t-submit'),
        log: document.getElementsByClassName('log'),
        clearme: document.getElementsByClassName('clear-me')
    };
    //Tools
    var utility = {
        compileTemplate: function (template) {
            //Create the Dom Element assing the the class and event
            function createElement(element, detail) {
                var domElement = document.createElement(element);
                if (detail.class) {
                    domElement.className = detail.class;
                }
                if (detail.text) {
                    domElement.appendChild(document.createTextNode(detail.text));
                }
                if (detail.events) {
                    for (var event in detail.events) {
                        domElement.addEventListener(event, detail.events[event]);
                    }
                }
                return domElement;
            }
            //recursive function that interate the Json object
            function interator(object, parentDom) {
                for (var element in object) {
                    var currentElement = createElement(element, object[element]);
                    if (object[element].child) {
                        interator(object[element].child, currentElement);
                    }
                    if (parentDom) {
                        parentDom.appendChild(currentElement);
                    }
                }
                return currentElement;
            }
            //return the dom with child and event listerners
            return interator(template);
        },
        notification: (function () {
            var listOfNotifications = {};
            function _on(name, callback) {
                if (!listOfNotifications.hasOwnProperty(name)) {
                    listOfNotifications[name] = [];
                }
                var index = listOfNotifications[name].push(callback) - 1;
                return {
                    remove: function () {
                        delete listOfNotifications[name][index];
                    }
                };
            }
            function _emit(name, variable) {
                listOfNotifications[name].forEach(function (callback) {
                    callback(variable);
                });
            }
            //Public API
            return {
                on: _on,
                emit: _emit
            };
        })(),
        serialize: function (list) {
            var array = [], len = list.length;
            for (var index = 0; index < list.length; index++) {
                array.push({
                    index: index,
                    text: list.item(index).childNodes[0].innerText
                });
            }

            return array;
        }
    };
    //add the clicn event to the add button
    element.addButton.addEventListener('click', function () {
        //check if the input is not empty
        if (element.input.value !== '') {
            //This is the Callback to the click handler
            function eraseMe(e) {
                var target = e.target;
                var parentTarget = target.parentElement;
                window['ggg'] = target;
                utility.notification.emit('erase', parentTarget.childNodes[0].innerText);
                element.listOfInput[0].removeChild(parentTarget);
            }
            var Li = {
                li: {
                    class: 'item',
                    child: {
                        'div': {
                            class: 't-text',
                            text: element.input.value,
                            events: {}
                        },
                        'button': {
                            class: "t-erase-me",
                            text: 'Erase Me',
                            events: {
                                click: eraseMe //Event click its add here.
                            }
                        }
                    }
                }
            };
            //it add in the list of object
            element.listOfInput[0].appendChild(utility.compileTemplate(Li));
            //Emit Event
            utility.notification.emit('add', element.input.value);
            //clean the input.
            element.input.value = '';
        }
    });
    //Submit click event using plain ajax this will no work because there is any Server side app
    element.submit.addEventListener('click', function () {
        var data = JSON.stringify(utility.serialize(element.listOfInput[0].children));
        if (data !== '[]') {

            utility.notification.emit('submit', data);

            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'https://www.google.com', true);
            xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

            // data as JSON
            xhr.send(data);

            xhr.onloadend = function () {
                console.log('AJAX process is done')
            };
            xhr.onerror = function () {
                console.warn('This data ' + data + ' was sent to the server and does not work due any backend');
            };


        }
        else {
            console.warn('Any data to send to the server please add something to the list');
        }
    });
    //Clear the console.log box
    element.clearme[0].addEventListener('click', function () {
        element.log[0].innerHTML = '';
    });
    //Console.log box
    //ADD
    utility.notification.on('add', function (data) {
        var template = {
            div: {
                class: 'log-item',
                text: data + "/ was Added to the list / " + new Date()
            }
        };
        element.log[0].appendChild(utility.compileTemplate(template));
    });
    //ERASE
    utility.notification.on('erase', function (data) {
        var template = {
            div: {
                class: 'log-item',
                text: data + "/ was Erased from the list / " + new Date()
            }
        };
        element.log[0].appendChild(utility.compileTemplate(template));
    });
    //SUBMIT
    utility.notification.on('submit', function (data) {
        var template = {
            div: {
                class: 'log-item',
                text: 'Info sent to server: ' + data
            }
        };
        element.log[0].appendChild(utility.compileTemplate(template));
    });
})();